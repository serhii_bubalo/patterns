﻿namespace AbstractFactory.AbstractClasses
{
	public abstract class Cake
	{
		public string Name { get; protected set; }
	}
}