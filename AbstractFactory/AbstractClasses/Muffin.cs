﻿namespace AbstractFactory.AbstractClasses
{
	public abstract class Muffin
	{
		public string Name { get; protected set; }
	}
}