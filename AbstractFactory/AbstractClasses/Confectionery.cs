﻿using System.Collections.Generic;

namespace AbstractFactory.AbstractClasses
{
	// Визначимо базовий тип для будь кондитерської
	 
	public abstract class Confectionery
	{
		public abstract Cake MakeCake();
		public abstract List<Muffin> MakeMuffins(int number);
		public abstract List<Profiterole> MakeProfiteroles(int number);

		public abstract Cake KillCake();
	}
}