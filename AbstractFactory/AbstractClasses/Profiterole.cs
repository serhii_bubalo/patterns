﻿namespace AbstractFactory.AbstractClasses
{
	public abstract class Profiterole
	{
		public string Name { get; protected set; }
	}
}