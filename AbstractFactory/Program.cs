﻿using System;
using AbstractFactory.Services;

namespace AbstractFactory
{
	class Program
	{
		static void Main()
		{
			//створення екземпляра кондитерському цеху   
			var confectionery = new CityCenterConfectionery();

			//замовити солодощі з центру кондитерському цеху
			Console.WriteLine(confectionery.MakeCake().Name);

			var muffins = confectionery.MakeMuffins(7);
			foreach (var m in muffins)
				Console.WriteLine(m.Name);

			var profiteroles = confectionery.MakeProfiteroles(5);
			foreach (var p in profiteroles)
				Console.WriteLine(p.Name);

			Console.ReadKey(true);
		}
	}
}
