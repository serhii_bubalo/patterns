﻿using System.Collections.Generic;
using AbstractFactory.AbstractClasses;

namespace AbstractFactory.Services
{
	// Реалызація фабрики продуктів

	public class CityCenterConfectionery : Confectionery
	{
		public override Cake MakeCake()
		{
			return new PragueCake("an amazing Prague Cake");
		}

		public override List<Muffin> MakeMuffins(int number)
		{
			List<Muffin> muffins = new List<Muffin>();
			for (var i = 0; i < number; i++)
				muffins.Add(new FrenchMuffin($"a beautiful French Muffin {i + 1}"));
			return muffins;
		}

		public override List<Profiterole> MakeProfiteroles(int number)
		{
			List<Profiterole> profiteroles = new List<Profiterole>();
			for (var i = 0; i < number; i++)
				profiteroles.Add(new ChocolateProfiterole($"a lovely Chocolate Profiterol {i + 1}"));
			return profiteroles;
		}

		public override Cake KillCake()
		{
			return new PragueCake("bad Cake");
		}
	}
}