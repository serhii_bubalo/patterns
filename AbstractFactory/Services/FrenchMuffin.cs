﻿using AbstractFactory.AbstractClasses;

namespace AbstractFactory.Services
{
	public class FrenchMuffin : Muffin
	{
		public FrenchMuffin(string name)
		{
			Name = name;
		}
	}
}