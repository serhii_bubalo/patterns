﻿using AbstractFactory.AbstractClasses;

namespace AbstractFactory.Services
{
	public class ChocolateProfiterole : Profiterole
	{
		public ChocolateProfiterole(string name)
		{
			Name = name;
		}
	}
}