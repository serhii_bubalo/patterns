﻿using AbstractFactory.AbstractClasses;

namespace AbstractFactory.Services
{
	public class PragueCake : Cake
	{
		public PragueCake(string name)
		{
			Name = name;
		}
	}
}