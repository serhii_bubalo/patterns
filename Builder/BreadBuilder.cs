﻿namespace Builder
{
	// абстрактний клас будівельника
	abstract class BreadBuilder
	{
		public Bread Bread { get; private set; }
		public abstract void SetWheatFlour();
		public abstract void SetRyeFlour();
		public abstract void SetSalt();
		public abstract void SetAdditives();

		public BreadBuilder()
		{
			Bread = new Bread();
		}
	}
}