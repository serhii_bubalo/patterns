﻿using System;
using System.Text;

namespace Builder
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			// кухар
			Baker baker = new Baker();
			
			// створюємо будівельника для житнього хліба
			BreadBuilder builder = new RyeBreadBuilder();
			// випікаємо хліб
			baker.Bake(builder);
			Bread ryeBread = builder.Bread;
			Console.WriteLine(ryeBread.ToString());

			// створюємо будівельника для пшеничного хліба
			builder = new WheatBreadBuilder();
			baker.Bake(builder);
			Bread wheatBread = builder.Bread;
			Console.WriteLine(wheatBread.ToString());

			Console.ReadKey(true);
		}
	}
}
