﻿namespace Builder
{
	class WheatBreadBuilder : BreadBuilder
	{
		public override void SetWheatFlour()
		{
			Bread.WheatFlour = new Flour { Sort = "вищий сорт" };
		}

		public override void SetRyeFlour()
		{

		}

		public override void SetSalt()
		{
			Bread.Salt = new Salt();
		}

		public override void SetAdditives()
		{
			Bread.Additives = new Additives { Name = "хлібопекарський поліпшувач" };
		}
	}
}