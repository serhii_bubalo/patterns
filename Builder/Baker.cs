﻿namespace Builder
{
	// Повар
	class Baker
	{
		public void Bake(BreadBuilder breadBuilder)
		{
			breadBuilder.SetWheatFlour();
			breadBuilder.SetRyeFlour();
			breadBuilder.SetSalt();
			breadBuilder.SetAdditives();
		}
	}
}