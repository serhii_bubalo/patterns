﻿using System.Text;

namespace Builder
{
	class Bread
	{
		public Flour WheatFlour { get; set; }
		public Flour RyeFlour { get; set; }
		public Salt Salt { get; set; }
		public Additives Additives { get; set; }

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			if (WheatFlour != null)
				builder.Append($"Пшеничная мука {WheatFlour.Sort}\n");
			if (RyeFlour != null)
				builder.Append($"Ржаная мука {RyeFlour.Sort} \n");
			if (Salt != null)
				builder.Append("Соль \n");
			if (Additives != null)
				builder.Append($"Добавки: {Additives.Name} \n");
			return builder.ToString();
		}
	}
}