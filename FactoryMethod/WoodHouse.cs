﻿using System;

namespace FactoryMethod
{
	class WoodHouse : House
	{
		public WoodHouse()
		{
			Console.WriteLine("Дерев'яний будинок побудований");
		}
	}
}