﻿namespace FactoryMethod
{
	// Абстрактний клас будівельної компанії
	abstract class Developer
	{
		public string Name { get; set; }

		public Developer(string name)
		{
			Name = name;
		}

		// фабричний метод
		public abstract House Create();
	}
}