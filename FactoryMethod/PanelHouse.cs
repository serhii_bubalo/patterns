﻿using System;

namespace FactoryMethod
{
	class PanelHouse : House
	{
		public PanelHouse()
		{
			Console.WriteLine("Панельний будинок побудований");
		}
	}
}