﻿namespace FactoryMethod
{
	// Будує панельні будинки
	class PanelDeveloper : Developer
	{
		public PanelDeveloper(string name) : base(name)
		{ }

		public override House Create()
		{
			return new PanelHouse();
		}
	}
}