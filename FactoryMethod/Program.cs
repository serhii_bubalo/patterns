﻿using System;
using System.Text;

namespace FactoryMethod
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;

			Developer developer = new PanelDeveloper("OOO Developer");
			developer.Create();

			developer = new WoodDeveloper("OOO QA");
			developer.Create();

			Console.ReadLine();
		}
	}
}
